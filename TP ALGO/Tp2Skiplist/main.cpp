// LIFAP6 - Automne 2017 - R. Chaine

#include "element.h"
#include "liste.h"
#include <cstdio>
#include <iostream>

int main()
{
    Liste lili;
    std::printf("Lili en cons /def\n  ");
    lili.affichage();
    unsigned int i;
    for(i = 0; i < 5; i++)
    {
        lili.ajoutElement(i);
    }
    std::printf("\nLili avant delete\n  ");
    lili.affichage();
    lili.suppressionEnTete();
    std::printf("\nLili apres delete\n  ");
    lili.affichage();
    Liste lolo(lili);
    std::printf("\nListe lolo:");
    lolo.affichage();
    lolo=lili;
    std::printf("\nLolo=Lili\n ");
    lolo.affichage();
    printf("done");



    return 0;
}
