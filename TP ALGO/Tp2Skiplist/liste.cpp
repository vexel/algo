
// LIFAP6 - Automne 2018

#include "element.h" //offrant le type Elem
#include "liste.h" //offrant le type liste
#include <iostream>

// -----------------------Listes Chain�es Tri�es----------------------------

//Constructeurs-------------------------------------------------------------
Liste::Liste()
{
    Cellule * bidon = new Cellule;
    ad = bidon;

    taille = 0;
    bidon->info=0;
    bidon->suivant=0;
}


Liste::Liste(const Liste & l)
{


    //Cr�ation d'une cellule bidon dans les deux cas
    Cellule * bidon = new Cellule;
    ad = bidon;
    ad->suivant = 0;

    if(l.taille != 0) //liste vide = pointeur de bidon � 0
    {
        taille = 0;
        Cellule * celltmp = l.ad->suivant; //Cellule de travail
        for (int i = 0; i < l.taille; i++)
        {
            ajoutElement(celltmp->info);
            celltmp = celltmp->suivant;
        }
    }
    else
    {
        taille=0;
    }
}

//Destructeur---------------------------------------------------------------
Liste::~Liste()
{
    vide(); //Suppressionde toute la liste
    delete ad;  //Suppression de la cellule bidon
}

void Liste::suppressionEnTete()
{
  Cellule *celltmp = ad->suivant; //On va sur Bidon
  ad->suivant=celltmp->suivant; //this->ad=this->ad->suivant; La deuxieme Cellule de *this devient la premiere
  delete celltmp;//L'espace occupe par la Cellule abandonnee est restitue
  taille--;
}

void Liste::vide()
{
    while(!testVide())
    {
      suppressionEnTete();
    }
}

bool Liste::testVide() const
{
    return (ad->suivant == 0);
}


//Affectation et Gestion------------------------------------------------------

void Liste::affichage() const
{
    Cellule * tmp = ad->suivant;


    if (ad->suivant == 0)
    {
        std::cout<<"La liste est vide \n";
    }
    else
    {
        int T = taille;
        std::cout<<"L={"<<tmp->info;
        while (tmp->suivant!=0 && T > 1)
        {
            tmp = tmp->suivant;
            std::cout<<","<<tmp->info;
            T--;
        }
        std::cout<<"} \n";
    }
}

void Liste::ajoutElement(const Elem & e)
{
    Cellule * newcell = new Cellule;
    Cellule * ptcell = ad;

    newcell->info = e;

    if(ptcell->suivant == 0) //bidon.suivant == 0 ?!
    {
        ad->suivant = newcell;
        newcell->suivant = 0;
    }
    else
    {
        while(ptcell->suivant!=0 && e < ptcell->suivant->info)
        {
            ptcell = ptcell->suivant;
        }

        //ajout de la cellule
        newcell->suivant=ptcell->suivant;
        ptcell->suivant=newcell;
    }

    taille++;
}

Liste & Liste::operator=(const Liste & l)
{
    if (this != &l) //Les listes ne sont pas les m�mes
    {
        this->vide();
        if(!l.testVide())
        {
            Cellule * celltmp = l.ad->suivant;
            do
            {
              ajoutElement(celltmp->info);
              celltmp = celltmp->suivant;
            }
            while(celltmp != 0);
        }
    }
  return *this;
}



//---------------------------Skip-Listes---------------------------------

//Constructeurs ---------------------------------------------------------

//Premiere cellule : c sentinelle
SListe::SListe()
{
    SCellule * bidon = new SCellule;
    ad = bidon;
    ad->nbniveau = MAX_NIVEAU;

    taille = 0;
    bidon->info=0;
    for(int i=0; i<5; i++)
    {
        bidon->suivant[i]=0;
    }
}


//init Sliste puis copie liste f(l)
SListe::SListe(const Liste & l)
{
    SCellule * bidon = new SCellule;
    ad = bidon;
    ad->nbniveau = MAX_NIVEAU;
    for(int i=0; i<5; i++)
    {
        bidon->suivant[i]=0;
    }

    if(l.taille != 0)
    {
        taille = 0;
        Cellule * celltmp = l.ad->suivant; //Cellule de travail partant de sentinel
        for (int i = 0; i < l.taille; i++)
        {
            ajoutElement(celltmp->info);
            celltmp = celltmp->suivant;
        }
    }
    else
    {
        taille=0;
    }
}

//Destructeurs ----------------------------------------------------------
SListe::~SListe()
{

}

//Gestion ---------------------------------------------------------------
void SListe::affichage() const
{
     SCellule * tmp = ad->suivant[0];


    if (ad->suivant[0] == 0)
    {
        std::cout<<"La liste est vide \n";
    }
    else
    {
        int T = taille;
        std::cout<<"\n  B";
        for (int i = 0; i < ad->nbniveau; i++)
        {
            std::cout<<" ["<<i<<"]";
        }
        std::cout<<std::endl;

        do
        {
            std::cout<<"  "<<tmp->info;
            for (int i = 0; i < MAX_NIVEAU; i++)
            {
                if (i < tmp->nbniveau)
                {
                    std::cout<<" ["<<i<<"]";
                }
            }
            tmp = tmp->suivant[0];
            T--;
            std::cout<<std::endl;
        }
        while (tmp!=0);
        std::cout<<"\n";
    }
}




void SListe::ajoutElement(const Elem & e)
{
    SCellule * newcell = new SCellule;
    SCellule * ptcell = ad;

    for (int i = 0; i < 5; i++) //initialisation de la nouvelle cellule
    {
        newcell->suivant[i] = 0;
    }
    newcell->info = e;
    newcell->nbniveau = 1; //Attention ! Probablement � modifier

    if(ptcell->suivant[0] == 0) //bidon.suivant == 0 ?!
    {
        ad->suivant[0] = newcell;
        newcell->suivant[0] = 0;
    }
    else
    {
        while(ptcell->suivant[0]!=0 && e < ptcell->suivant[0]->info) //Placement du curseur au bon endroit
        {
            ptcell = ptcell->suivant[0];
        }

        //ajout de la c�llule
        newcell->suivant[0]=ptcell->suivant[0];
        ptcell->suivant[0]=newcell;
    }

    taille++;
}

void SListe::Skipping()
{
    //On a besoin de deux pointeurs, un en arri�re et un qui parcours
    SCellule * ptrarr;
    SCellule * ptrav = ad->suivant[0];
    int pf; //Pile ou Face, valeur = 0 ou  1

    for (int etage = 1; etage < MAX_NIVEAU; etage++) //Cr�ation de tous les �tages, etage 0 d�j� cr��
    {
        ptrarr = ad->suivant[etage]; //reste sur la cellule bidon au d�but
        ptrav = ad->suivant[etage];
        for (int pos = 0; pos < taille; pos++)
        {
            pf = rand() % 2;
            if (pf == 1)
            {
                ptrarr = ptrav->suivant[0];
            }
        }
    }
}






