#include <iostream>
#include "SkipList.hpp"
#include "SkipListElement.hpp"


int main()
{
    SkipList sl( 0.5 , 100 );
    int size = 60;
    for( int i = 0 ; i < size ; i++ )
    {
        sl.add( SkipListElement( i ) );
    }
    sl.print();
    int sum = 0;
    for( int i = 0 ; i < size ; i++ )
    {
        sum += sl.stepsToFind(i);
    }
    std::cout << sum / ((double)size) << std::endl;

}


void test()
{
    double sumavg = 0;
    for( int tests = 0 ; tests < 1000 ; tests++ )
    {
        SkipList sl( 0.42 , 100 );
        for( int i = 0 ; i < 1000000 ; i++ )
        {
            sl.add( SkipListElement( i ) );
        }
        //sl.print();
        int sum = 0;
        for( int i = 0 ; i < 1000000 ; i++ )
        {
            sum += sl.stepsToFind(i);
        }
        sumavg += sum/1000000.0;
        std::cout << "avg = " << sumavg / (tests + 1) << std::endl ;
    }
}
