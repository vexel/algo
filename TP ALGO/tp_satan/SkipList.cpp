
#include "SkipList.hpp"

#include <cstdlib>
#include <ctime>
#include <string>
#include <iostream>


SkipListCell::SkipListCell( int ptrsNumber, SkipListElement element ) : mElement( element ) , mHeight( ptrsNumber )
{
    mNextCells = new SkipListCell*[ptrsNumber];
    for( int i = 0 ; i < ptrsNumber ; i++ )
        mNextCells[i] = nullptr;
}
SkipListCell::~SkipListCell()
{
    delete mNextCells;
}

int coinToss( double p , int max )
{
    int i = 0;
    int n = p * 1000;
    while ( std::rand()%1000 < n  && i < max) i++;
    return i;
}

SkipList::SkipList( double p , int maxHeight) : mP( p ) , mMaxHeight( maxHeight ) , mMaxReachedHeight( 0 )
{
    mFirstCell = new SkipListCell( mMaxHeight , SkipListElement( -1 ) );
    std::srand( time( NULL ) );
}

SkipList::~SkipList()
{
    SkipListCell * cell = mFirstCell , * del;
    while( cell )
    {
        del = cell;
        cell = cell->mNextCells[0];
        delete del;
    }
}

void SkipList::add( SkipListElement e )
{
    SkipListCell * newCell = new SkipListCell( 1 + coinToss( mP , mMaxHeight - 1 ) , e );
    mMaxReachedHeight = ( newCell->mHeight - 1 > mMaxReachedHeight ) ? newCell->mHeight - 1 : mMaxReachedHeight;
    SkipListCell * currentCell = mFirstCell;
    int currentLevel = mMaxHeight - 1;
    bool done = false;
    while( !done )
    {
        if( ( currentCell->mNextCells[currentLevel]  != nullptr ) && ( currentCell->mNextCells[currentLevel]->mElement < e ) ) // forward case
            currentCell = currentCell->mNextCells[currentLevel];
        else //down case
        {
            if( currentLevel < newCell->mHeight )
            {
                newCell->mNextCells[currentLevel] = currentCell->mNextCells[currentLevel];
                currentCell->mNextCells[currentLevel] = newCell;
            }
            if ( currentLevel != 0 )
                    currentLevel--;
            else
                done = true;
        }
    }
}

void SkipList::print()
{
    SkipListCell * cell, * cellIt;
    std::string line;
    for( int level = maxReachedHeight() ; level >= 0 ; level-- )
    {
        line = "";
        cell = mFirstCell;
        while( cell->mNextCells[level] != nullptr )
        {
            cellIt = cell->mNextCells[0];
            while( cellIt != cell->mNextCells[level] )
            {
                line += std::string(  std::to_string( cellIt->mElement.mValue ).length() + 1 , '-' );
                cellIt = cellIt->mNextCells[ 0 ];
            }
            line += "-" + std::to_string( cellIt->mElement.mValue );
            cell = cell->mNextCells[level];
        }
        std::cout << line << std:: endl;

    }
}

int SkipList::maxReachedHeight()
{
    int i = mMaxHeight-1;
    while( mFirstCell->mNextCells[i] == nullptr )
        i--;

    return i;
}

void SkipList::print2()
{
    SkipListCell * cell = mFirstCell;
    while( cell->mNextCells[0] != nullptr )
    {
        for( int i = 0 ; i < cell->mNextCells[0]->mHeight ; i++ )
        {
            std::cout << std::endl << cell->mNextCells[0]->mElement.mValue << "[" << i << "] = ";
            if( cell->mNextCells[0]->mNextCells[i] != nullptr )
                std::cout << cell->mNextCells[0]->mNextCells[i]->mElement.mValue;
            else
                std::cout << "null";
        }
        cell = cell->mNextCells[0];
    }
}

int SkipList::stepsToFind( int n )
{

    SkipListCell * currentCell = mFirstCell;
    int currentLevel = mMaxReachedHeight;
    bool found = false;
    int step = 0;
    while( currentLevel >= 0  && !found )
    {
        if( ( currentCell->mNextCells[currentLevel]  != nullptr ) && ( currentCell->mNextCells[currentLevel]->mElement.mValue <= n ) ) // forward case
        {
            currentCell = currentCell->mNextCells[currentLevel];
            if( currentCell->mElement.mValue == n )
            {
                found = true;
            }
        }
        else //down case
        {
            currentLevel--;
        }
         step++;
    }
    return ( (found) ? step : -1 );
}


