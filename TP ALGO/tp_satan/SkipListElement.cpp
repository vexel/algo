#include "SkipListElement.hpp"

bool SkipListElement::operator<( const SkipListElement & sl )
{
    return ( mValue < sl.mValue );
}

bool SkipListElement::operator>( const SkipListElement & sl )
{
    return ( mValue > sl.mValue );
}
