#ifndef SKIPLIST_HPP
#define SKIPLIST_HPP

#include "SkipListElement.hpp"

class SkipListCell
{
    friend class SkipList;
    public:

        SkipListCell( int ptrsNumber , SkipListElement e );
        ~SkipListCell();
    private:
        SkipListElement mElement;
        SkipListCell ** mNextCells;
        int mHeight;
};

class SkipList
{
    public:
        SkipList( double p , int maxHeight);
        ~SkipList();

        void add( SkipListElement e);
        void print();
        void print2();
        int maxReachedHeight();
        int stepsToFind( int n );

    private:
        SkipListCell * last();

        SkipListCell * mFirstCell;
        double mP;
        int mMaxHeight;
        int mMaxReachedHeight;
};

#endif
