/**
 * @file main.cpp
 * @authors Vincent KLEINBAUER
 * @authors Jonathan RAYBAUD--SERDA
*/

#include <iostream>
#include <fstream>
#include <chrono>
#include <stdlib.h>
#include <time.h>

#include "Table.h"
#include "Element.h"
#include "hashFunctions.h"
#include "reHashFunctions.h"

const int NB_KEYS = 200;

Element createElement(key k, info i)
{
    Element e;
    e.k = k;
    e.i = i;
    return e;
}

void performanceTest()
{
    std::ofstream file;
    file.open("performance.txt", std::ios::app);
    file << "#nb    " << "Time" << std::endl;

    for(unsigned int i = 1000; i <= MAX_SIZE; i += 1000)
    {
        Table t(i, &sumFactorialDigitHash, &linearRehash);
        std::cout << "Table created with size " << i << std::endl;
        
        for(unsigned int j = 0; j < t.getSize(); j++)
        {
            t.insertElement(createElement(rand() % 10000 + 1, rand() % 50000 + 1));
        }

        std::chrono::time_point<std::chrono::system_clock> start, end;
        int searchTimesSum = 0;

        for(int k = 0; k <= NB_KEYS; k++)
        {
            start = std::chrono::system_clock::now();
            if(t.searchKey(rand() % 10000 + 1))
            {
                std::cout << "Key found !" << std::endl;
            }
            else
            {
                std::cout << "Key not found." << std::endl;
            }
            end = std::chrono::system_clock::now();
            searchTimesSum += std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
        }

        float averageTime = searchTimesSum / NB_KEYS;
        std::cout << "Table size " << i << ", average time for key search " << averageTime << std::endl;
        file << i << " " << averageTime << std::endl;
    }

    file.close();
}

int main(int argc, char ** argv)
{
	
    srand(time(NULL));
    
    int size;
    int hashFunctionChoice = 4;
    int rehashFunctionChoice = 4;
    
    std::cout<<"Chose the size of the hashtable"<<std::endl;
    scanf("%d",&size);
    
    std::cout<<std::endl;
    while(hashFunctionChoice >3 || hashFunctionChoice <1){
		std::cout<<"Chose the hashFunction"<<std::endl;
		std::cout<<"(1) moduloHash"<<std::endl;
		std::cout<<"(2) productDigitHash"<<std::endl;
		std::cout<<"(3) sumFactorialDigitHash"<<std::endl;
		scanf("%d",&hashFunctionChoice);
	}
	std::cout<<std::endl;
	while(rehashFunctionChoice >3 || rehashFunctionChoice <1){
		std::cout<<"Chose the rehashFunction"<<std::endl;
		std::cout<<"(1) linearRehash"<<std::endl;
		std::cout<<"(2) quadraticRehash"<<std::endl;
		std::cout<<"(3) doubleRehash"<<std::endl;
		scanf("%d",&rehashFunctionChoice);
	}

	
	//~ typedef int (*hashFunction) (key k, unsigned int size);
    //~ hashFunction possibilities[] = {moduloHash,productDigitHash,sumFactorialDigitHash};
    //~ 
	//~ typedef int (*rehashFunction) (Element hashTable[],key k, int size);
    //~ rehashFunction possibilities1[] = {linearRehash,quadraticRehash,doubleRehash};
    //~ 
    //~ Table t(size,possibilities[hashFunctionChoice],possibilities1[rehashFunctionChoice]);
	//~ 
    int choice;
    while(choice != 5){
		choice = -1;
		std::cout<<std::endl<<std::endl<<std::endl<<std::endl;
		while(choice <1 || choice >5){
			std::cout<<"Chose what to do"<<std::endl;
			std::cout<<"(1) Insert element"<<std::endl;
			std::cout<<"(2) Delete element"<<std::endl;
			std::cout<<"(3) Search from key"<<std::endl;
			std::cout<<"(4) Display table"<<std::endl;
			std::cout<<"(5) exit"<<std::endl;
			scanf("%d",&choice);
			std::cout<<std::endl<<std::endl<<std::endl<<std::endl;
		}
		key k;
		switch(choice){
			case 1:
				int info;
				std::cout<<"Create the element to insert"<<std::endl;
				std::cout<<"Chose key"<<std::endl;
				scanf("%d",&k);
				std::cout<<"Chose info"<<std::endl;
				scanf("%d",&info);
				t.insertElement(createElement(k, info));
				break;
			case 2:
				std::cout<<"Chose key"<<std::endl;
				scanf("%d",&k);
				t.removeElement(createElement(k,-1));
				break;
			case 3:
				std::cout<<"Chose key"<<std::endl;
				scanf("%d",&k);
				if(t.searchKey(5555))
				{
					std::cout << "Key Found !" << std::endl;
				}
				else
				{
					std::cout << "Key not Found." << std::endl;
				}
				break;
			case 4:
				t.display();
				break;
			default:
				continue;
			}
		}
			
			
			
			
		
		
		
    //~ Table t(5, &moduloHash, &doubleRehash);
//~ 
    //~ t.insertElement(createElement(5555, 50));
    //~ t.insertElement(createElement(555, 46));
    //~ t.insertElement(createElement(42, 42));
    //~ t.insertElement(createElement(4, 101));
    //~ t.insertElement(createElement(1112, 98));
    //~ t.insertElement(createElement(1111, 98));
//~ 
    //~ t.display();
//~ 
    //~ std::cout << std::endl;
    //~ std::cout << "Search operation test" << std::endl;
    //~ if(t.searchKey(5555))
    //~ {
        //~ std::cout << "Key 3 Found !" << std::endl;
    //~ }
    //~ else
    //~ {
        //~ std::cout << "Key 3 not Found." << std::endl;
    //~ }
//~ 
    //~ std::cout << std::endl;
    //~ std::cout << "Deleting element with key 3 :" << std::endl;
    //~ t.removeElement(createElement(3, 46));
//~ 
    //~ std::cout << std::endl;
    //~ t.display();

    return 0;
}
