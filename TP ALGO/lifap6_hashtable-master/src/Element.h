/**
 * @file Element.h
 * @authors Vincent KLEINBAUER
 * @authors Jonathan RAYBAUD--SERDA
*/

#ifndef ELEMENT_H
#define ELEMENT_H

/**
 * @typedef unsigned int key
 * 
 * Defines a type `key`, which in this case is an `unsigned int`. This system allows the user
 * to change the key type to something else.
*/
typedef unsigned int key;

/**
 * @typedef float info
 * 
 * Defines a `info` type which is used to specify the type of the information stored in each cell of our
 * hash table.
*/
typedef float info;

/**
 * @struct Element
 * @brief Defines the `Element` which will be stored in our Hash Table
 * 
 * By default, each cell of our Hash Table will be filled with an `Element`. Each `Element` is comprised of three attributes which allows us to
 * store, in each cell, a key, a value and a boolean variable to track wether or not this particular cell has already been used.
 * 
*/
struct Element
{
    Element() : k(0), i(0), hasBeenFilled(false){}
    /**
     * @public The Element's key
    */
    key k;
    /**
     * @public The Element's information, in other words, the actual information we want to store in a cell
    */
    info i;
    /**
     * @public boolean value used to track wether or not this particular Element has already received a value or not
    */
    bool hasBeenFilled;
};

#endif