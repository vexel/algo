/**
 * @file hashFunctions.h
 * @authors Vincent KLEINBAUER
 * @authors Jonathan RAYBAUD--SERDA
*/

#ifndef HASH_FUNCTIONS_H
#define HASH_FUNCTIONS_H

#include "Element.h"

/**
 * @fn int moduloHash(key k, unsigned int size)
 * 
 * This hash function is used to generate an integer from the key given as a parameter.
 * This particular one does so by calculating :
 * ```
 * key % size
 * ```
 * 
 * @param k the key to hash, of type `key`
 * @param size the size of the hash table
 * @return an unsigned int
*/
int moduloHash(key k, unsigned int size);


/**
 * @fn int productDigitHash(key k, unsigned int size) 
 * 
 * given a number the function return a product of all digits in it
 * this number must be smaller than the size of the table so we apply a modulo
 * 
 * 111 return (1*1*1) % size = 1 % size
 * 120 return (1*2*0) % size = 0
 * 
 * @param k the key to hash, of type `key`
 * @param size the size of the hash table
 * @return an unsigned int
*/
int productDigitHash(key k, unsigned int size) ;

/**
 * @fn int sumFactorialDigitHash(key k, unsigned int size) 
 * 
 * given a number the function return a sum of all factorial from all digits in the number
 * this number must be smaller than the size of the table so we apply a modulo
 * 
 * 111 return (1!+1!+1!) % size = 3 % size
 * 120 return (1!+2!+0!) % size = 4 % size
 * 
 * @param k the key to hash, of type `key`
 * @param size the size of the hash table
 * @return an unsigned int
*/
int sumFactorialDigitHash(key k,unsigned int size);

#endif
