/**
 * @file hashFunctions.cpp
 * @authors Vincent KLEINBAUER
 * @authors Jonathan RAYBAUD--SERDA
*/

#include "Element.h"
#include "hashFunctions.h"
#include <iostream>
#include <fstream>
#include <chrono>
#include <stdlib.h>
#include <time.h>

int moduloHash(key k, unsigned int size)
{	
    return k % size;
}



int productDigitHash(key k, unsigned int size) 
{ 
    unsigned int digit_sum = 1; 
    while (k > 0)  
    { 
        int rem = k % 10; 
        digit_sum = digit_sum * rem; 
        k = k / 10; 
    }
    return digit_sum% size;
    
}
int sumFactorialDigitHash(key k, unsigned int size) 
{ 

    unsigned int res = 0; 
    while (k > 0)  
    { 
		int factorial = 1;
        int rem = k % 10;
        
        //factorial operator
        for(int i =1;i<=rem;++i){
			factorial *= i;
		}
		
        res = res + factorial; 
        k = k / 10;
    }
    return res%size;
}

