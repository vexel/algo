#ifndef RE_HASH_FUNCTIONS_H
#define RE_HASH_FUNCTIONS_H

#include "Element.h"

/**
 * @fn llinearRehash(Element hashTable[],key k, int size);
 * 
 * while there is a collision in the index gived in parameter a simple iteration search for an empty cell
 * if there is not the function  return -1 else the index of the latter

 * @param Element hashTable[] the array where there is a collision
 * @param k the key to hash, of type `key`
 * @param size the size of the hash table
 * @return an int
*/
int linearRehash(Element hashTable[],key k, int size);


/**
 * @fn quadraticRehash(Element hashTable[],key k, int size);
 * 
 * while there is a collision in the index gived in parameter an iterator going from square to square search for an empty cell (1,4,9,25...)
 * if there is not the function  return -1 else the index of the latter

 * @param Element hashTable[] the array where there is a collision
 * @param k the key to hash, of type `key`
 * @param size the size of the hash table
 * @return an int
*/
int quadraticRehash(Element hashTable[],key k, int size);


/**
 * @fn doubleRehash(Element hashTable[],key k, int size);
 * 
 * while there is a collision in the index gived in parameter a rehash is made with a new modulo different of the size and being a prime number
 * if the result as an index is filled in the hashtable, it gets incremented by 5 size times
 * if there is not the function  return -1 else the index of the latter

 * @param Element hashTable[] the array where there is a collision
 * @param k the key to hash, of type `key`
 * @param size the size of the hash table
 * @return an int
*/
int doubleRehash(Element hashTable[],key k, int size);
#endif
