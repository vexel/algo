/**
 * @file Table.h
 * @authors Vincent KLEINBAUER
 * @authors Jonathan RAYBAUD--SERDA
*/

#ifndef TABLE_H
#define TABLE_H

#include "Element.h"

/**
 * @var MAX_SIZE is a constant used to specify the maximum size that a Hash Table can have
*/
const int MAX_SIZE = 10000;

/**
 * @class Table
 * @brief Defines the Table abstract type which is based on a Hash Table typed implementation
*/
class Table
{
    public:
        /**
         * @public The default constructor for the `Table` class
         * 
         * @param size is used to specify the size of the Hash Table we want to create. Note that if the size specified
         * is greater than the maximum size (defined by the MAX_SIZE constant), its value will be equal to that of MAX_SIZE.
         * @param hashFunction is a pointer to the hash function we want to use with this particular instance of the Table class.
         * @param reHashFunction is a pointer to the re-hash function we want to use with this particular instance of the Table class.
        */
        
        Table(unsigned int size, int (*hashFunction)(key, unsigned int), int (*reHashFunction)(Element*,key, int));
        /**
         * @public The destructor for the Table class.
        */
        ~Table();

        /**
         * @public Returns the size of the Table
         * 
         * @return an unsigned int containing the size of the Table object considered
        */
        unsigned int getSize() const;
        
           /**
         * @public Returns the Table
         * 
         * @return a pointer of the first element of the Table
        */
        Element *getArray() const;
        

        /**
         * @public inserts the Element given in parameter into the Table
         * @param e The Element to insert
         * @return true if the insertion has been successful, false otherwise
        */

        
        bool insertElement(Element e);
        /**
         * @public Removes the specified element from the Table
         * @param e The Element to remove
        */
        void removeElement(Element e);
        /**
         * @public Search for the key given in parameter in the Table
         * @param k The key to look for
         * @return true if the given key is present in the Table, false otherwise
        */
        bool searchKey(key k);
        /**
         * @public Retrieves the information associated with the key given in parameter
         * @param k The key associated with the Element which we want the value of
         * @return The information associated with the Key given in parameter
        */
        info getInfoFromKey(key k);
        /**
         * @public Allows modification of the information associated to a Key
         * @param k The Key associated with the Element we want to modify the value of
         * @param i The new information to store
        */
        void modifyInformationFromKey(key k, info i);

        /**
         * @public Displays the Table
        */
        void display() const;

    private:
        void setSize(unsigned int size);

        unsigned int m_size;
        Element * m_array;
        int (*m_hashFunction)(key, unsigned int);
        int (*m_reHashFunction)(Element*,key, int);
};

#endif
