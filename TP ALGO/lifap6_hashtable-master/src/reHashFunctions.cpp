#include "Element.h"
#include "reHashFunctions.h"
#include <iostream>

int linearRehash(Element hashTable[],key k, int size){
	int iterationCounter = 0;
	key originalKey = k;
	while((hashTable[k%size].hasBeenFilled && iterationCounter < size)&&hashTable[k%size].k!=originalKey){
		k = (k + 1)%size ;
		iterationCounter++;
		
	}
	if(hashTable[k].hasBeenFilled && hashTable[k%size].k!=originalKey)
		return -1;
	std::cout<<k%size<<std::endl;
	return k%size;
}

int quadraticRehash(Element hashTable[],key k, int size){
	int inc = 1;
	key originalKey = k;
	while((inc<=size && hashTable[(k+(inc*inc))%size].hasBeenFilled) && hashTable[(k+(inc*inc))%size].k!=originalKey){
		inc++;
	}	
	if(hashTable[(k+(inc*inc))%size].hasBeenFilled  && hashTable[(k+(inc*inc))%size].k!=originalKey)
		return -1;
	return (k+(inc*inc))%size;

}

int doubleRehash(Element hashTable[],key k, int size){
	key originalKey = k;
	int res;
	int iterationCounter=0;
	if(k == 0)
		return -1;
	if(size==19)
	{
		res = (k%17)%size;
		while(hashTable[res%size].hasBeenFilled && iterationCounter < size){
			res += 3;
			iterationCounter++;
		}
		if(hashTable[res%size].hasBeenFilled && hashTable[res%size].k != originalKey){
			return -1;
		}
		return res%size;
	}
	
	
	res = (k%19)%size;
	iterationCounter = 0;
	
	while(hashTable[res%size].hasBeenFilled && iterationCounter < size){
		res += 3;
		iterationCounter++;
	}
	if(hashTable[res%size].hasBeenFilled && hashTable[res%size].k != originalKey){
		return -1;
	}
	return res%size;
}
