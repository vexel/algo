/**
 * @file Table.cpp
 * @authors Vincent KLEINBAUER
 * @authors Jonathan RAYBAUD--SERDA
*/

#include <iostream>
#include <iomanip> 
#include "Table.h"
#include "Element.h"
using std::setw;


Table::Table(unsigned int size, int (*hashFunction)(key, unsigned int), int (*reHashFunction)(Element*,key, int))
{
    if(size > MAX_SIZE)
    {
        this->setSize(MAX_SIZE);
    }
    else
    {
        this->setSize(size);
    }
    
    m_array = new Element [this->getSize()];
    m_hashFunction = hashFunction;
    m_reHashFunction = reHashFunction;
    std::cout << "[INFO] Table constructed successfully." << std::endl;
}

Table::~Table()
{
    delete m_array;
    m_array = nullptr;
    std::cout << "[INFO] Table destructed successfully." << std::endl;
}

unsigned int Table::getSize() const
{
    return this->m_size;
}

Element* Table::getArray() const
{
	return this->m_array;
}

void Table::setSize(unsigned int size)
{
    this->m_size = size;
}

bool Table::insertElement(Element e)
{	
	
    int hashValue = m_hashFunction(e.k, this->getSize());
	if(!this->m_array[hashValue].hasBeenFilled)
	{
		this->m_array[hashValue].i = e.i;
		this->m_array[hashValue].k = e.k;
		this->m_array[hashValue].hasBeenFilled = true;
		return true;
	}
	
	else if(this->m_array[hashValue].k == e.k)
	{

		this->m_array[hashValue].i = e.i;
		return true;
	}
	
	else
	{
		hashValue = m_reHashFunction(this->getArray(), e.k, this->getSize());
		if(hashValue != -1){
			this->m_array[hashValue].i = e.i;
			this->m_array[hashValue].k = e.k;
			this->m_array[hashValue].hasBeenFilled = true;
		}
		return hashValue != -1;
	}

}

void Table::removeElement(Element e)
{
    if(this->searchKey(e.k))
    {
        this->m_array[this->m_hashFunction(e.k, this->getSize())].i = 0;
        this->m_array[this->m_hashFunction(e.k, this->getSize())].k = 0;
    }
}

bool Table::searchKey(key k)
{
    int hashValue = m_hashFunction(k, this->getSize()); 
	std::cout<<"---"<<hashValue<<std::endl;
	if(!this->m_array[hashValue].hasBeenFilled)
	{
		std::cout<<"11"<<std::endl;
		return false;
	}
	else
	{
		if(this->m_array[hashValue].k == k)
		{
			return true;
			std::cout<<"111"<<std::endl;
		}
		else
		{
			std::cout<<"1111"<<std::endl;
			hashValue = m_reHashFunction(this->getArray(), k, this->getSize());
			return hashValue != -1;
			
		}
	}

}

info Table::getInfoFromKey(key k)
{
    return this->m_array[this->m_hashFunction(k, this->getSize())].i;
}

void Table::modifyInformationFromKey(key k, info i)
{
    this->m_array[this->m_hashFunction(k, this->getSize())].i = i;
}

void Table::display() const
{
    for(unsigned int i = 0; i < this->getSize(); i++)
    {
		std::cout << i << " Key : "<< std::setw(3) <<  this->m_array[i].k 
				  << "   Info :" <<std::setw(3)<< this->m_array[i].i 
				  << "   Filled ? -> "<< std::boolalpha << m_array[i].hasBeenFilled
				  << std::endl;
    }
}
