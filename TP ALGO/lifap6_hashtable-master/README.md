# LIFAP6 - Graded Practice work

This project is realized as part of the LIFAP6 course, autumn 2018 session. The goal is to implement a structure for storing elements which will work on the principle of `Hash Tables`.

* Authors :
    * Vincent KLEINBAUER
    * Jonathan RAYBAUD--SERDA
* Language used : `C++`
* Deadline : 2018-11-05

----
## Project tree

The project tree is organized as follows :
```
lifap6_hashtable
    |
    |_____bin
    |_____doc
    |_____obj
    |_____src
    |
    |_____makefile
    |_____README.md
```

* The `bin` directory contain the executable resulting from the compiling process
* The `doc` directory contains the documentation, generated using `Doxygen`
* The `obj` directory contain object files, which are an intermediate product of the compiling process
* The `src` contains the project source code. Since this project is rather small, all of the files composing the source code are in the same folder.

----
## Generating the documentation

The documentation is generated from Doxygen-formatted comments present in the source code. To generate the documentation in HTML format, first open a terminal into the `doc` folder and then run doxygen as follows :
```shell
doxygen doxyfile
```

This commande will generate in the `doc` directory a `html` folder, in which you will find the `index.html` file. Open this file with a web browser to access the documentation.

----
## Compiling the project

To compile the project, first open a terminal in the project's root directory (`lifap6_hashtable`). Then, run `make` using the following command :
```shell
make
```

If needed, you can remove the object files and start the compilation process from scratch with the following command :
```shell
make clean
```

If you need to totally wipe out all the results of the compilation process and start anew, you can use the following command
```shell
make mrproper
```
which will remove the executable and the object files.