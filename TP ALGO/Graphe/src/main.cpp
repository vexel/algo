/**
 * @file main.cpp
 * @authors Vincent KLEINBAUER
*/

#include <iostream>
#include "Graphe.h"

int main(int argc, char ** argv)
{
	std::cout<<std::endl;
	Graphe g(5,5);
	g.dfs(0);
	g.displayColor();
	std::cout<<g.getEastFrom(0)<<std::endl;
    return 0;
}

