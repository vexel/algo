/**
 * @file Queue.h
 * @authors Vincent KLEINBAUER
*/

#ifndef QUEUE_H
#define QUEUE_H

/**
 * @class Queue
 * @brief 
*/


class Queue
{
    public:
		Queue(int size);
		void display();
		

    private:
		int* pred;
		int* dist;
		int size;

		void setDist();
};

#endif

