/**
 * @file Graphe.cpp
 * @authors Vincent KLEINBAUER
*/

#include <iostream>
#include <iomanip> 
#include <math.h>  
#include "Graphe.h"

using std::setw;


Graphe::Graphe(int width,int height)
{
	this->width = width;
	this->height = height;
	this->altitude = new int[width*height];
	this->color = new int[width*height];
	setAltitudeCellToZero();
	setColorCellToZero();
	generateHills();

}

Graphe::~Graphe()
{
    //~ delete m_array;
    //~ m_array = nullptr;
    std::cout << "[INFO] Table destructed successfully." << std::endl;
}

void Graphe::setAltitudeCellToZero(){
	for(int y=0;y<=this->height;y++){
		for(int x=0;x<=this->width;x++){
			this->altitude[x+y*width] = 0;
		}
	}
}

void Graphe::setColorCellToZero(){
	for(int y=0;y<=this->height;y++){
		for(int x=0;x<=this->width;x++){
			this->color[x+y*width] = 0;
		}
	}
}


void Graphe::display() const{
	for(int y=0;y<=this->height;y++){
		for(int x=0;x<=this->width;x++){
			std::cout<<std::setw(2)<<this->altitude[x+y*width];
		}
		std::cout<<std::endl;
	}
	std::cout<<std::endl;
}

void Graphe::displayColor() const{
	for(int y=0;y<=this->height;y++){
		for(int x=0;x<=this->width;x++){
			std::cout<<std::setw(2)<<this->color[x+y*width];
		}
		std::cout<<std::endl;
	}
	std::cout<<std::endl;
}

int* Graphe::getTwoDimArrayCoordFrom(int position){
	int arr[2];
	arr[0] = position/this->width;
	arr[1] = position%this->width;
	return arr;
	
}

int Graphe::getOneDimArrayCoordFrom(int aWidth,int aHeight){
	return aWidth + (aHeight * this->width);
}




int Graphe::getNorthFrom(int position){
	int y = getTwoDimArrayCoordFrom(position)[0];
	int x = getTwoDimArrayCoordFrom(position)[1];
	if(y != 0)
		return getOneDimArrayCoordFrom(x,y-1);
	return -1;
}
int Graphe::getSouthFrom(int position){
	int y = getTwoDimArrayCoordFrom(position)[0];
	int x = getTwoDimArrayCoordFrom(position)[1];
	if(y != this->height-1)
		return getOneDimArrayCoordFrom(x,y+1);
	return -1;
}
int Graphe::getEastFrom(int position){
	int y = getTwoDimArrayCoordFrom(position)[0];
	int x = getTwoDimArrayCoordFrom(position)[1];
	if(x != this->width-1)
		return getOneDimArrayCoordFrom(x+1,y);
	return -1;
}
int Graphe::getWestFrom(int position){
	int y = getTwoDimArrayCoordFrom(position)[0];
	int x = getTwoDimArrayCoordFrom(position)[1];
	if(x != 0)
		return getOneDimArrayCoordFrom(x-1,y);
	return -1;
}

//h pg -h pd

int Graphe::getDist(int coord1,int coord2){
	return sqrt(pow(this->altitude[coord1]-this->altitude[coord2],2));
}

void Graphe::generateHills(){
	int hillsNumber = rand()%100;

	for(int i=0;i<hillsNumber;i++){	
		int origineX = rand()%this->width;
		int origineY = rand()%this->height;
		int pos = getOneDimArrayCoordFrom(origineX,origineY);
		
		int north = getNorthFrom(pos);
		int south = getSouthFrom(pos);
		int west = getWestFrom(pos);
		int east = getEastFrom(pos);
		
		if(north != -1)
			this->altitude[north]+=1;
		if(south != -1)
			this->altitude[south]+=1;
		if(east != -1)
			this->altitude[east]+=1;
		if(west != -1)
			this->altitude[west]+=1;
			
		
		
		this->altitude[pos] +=  2;
		

	}
	display();	
}


void Graphe::dijkstra(int start,int end){
	setColorCellToZero();

	
}

void Graphe::dfs(int start){
	this->color[start]=2;
	for(int i = 0;i<4;i++){
		if(getWhiteAdj(start)[i]!=-1){
			/displayColor();	
			dfs(getWhiteAdj(start)[i]);
		}
			
	}
}

int* Graphe::getWhiteAdj(int start){
	int north = getNorthFrom(start);
	int south = getSouthFrom(start);
	int east = getEastFrom(start);
	int west = getWestFrom(start);
	

	int adj[4];
	
	if(north != -1 && this->color[north]==0)
		adj[0] = north;
	else
		adj[0] = -1;
		
	if(south != -1 && this->color[south]==0)
		adj[1] = south;
	else
		adj[1] = -1;
		
	if(east != -1 && this->color[east]==0)
		adj[2] = east;
	else
		adj[2] = -1;
		
	if(west != -1 && this->color[west]==0)
		adj[3] = west;
	else
		adj[3] = -1;
		

	return adj;
	
}





















//~ 
//~ Queue::Queue(int size){
			//~ this->pred = new int[size];
			//~ this->dist = new int[size];
			//~ this->size = size;
			//~ std::cout<<"ntmmmmmmmmmmmmmmmmmmmmmmmmm"<<std::endl;
			//~ 
				//~ for(int i = 0; i<this->size;i++){
		//~ std::cout<<i;}
	//~ std::cout<<std::endl;
	//~ 
	//~ for(int i = 0; i<this->size;i++){
		//~ std::cout<<this->pred;}
	//~ std::cout<<std::endl;
	//~ 
	//~ for(int i = 0; i<this->size;i++){
		//~ std::cout<<this	->dist;}
	//~ std::cout<<std::endl;
		//~ }
//~ 
//~ 
//~ void Queue::setDist(){	
			//~ for(int i=0; i<this->size ;i++){
					//~ this->dist[i] = 999999;
			//~ }
//}






