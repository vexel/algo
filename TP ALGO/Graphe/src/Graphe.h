/**
 * @file Graphe.h
 * @authors Vincent KLEINBAUER
*/

#ifndef GRAPHE_H
#define GRAPHE_H

/**
 * @class Graphe
 * @brief a graphe composed by width and height in order to handle a one dimentionnal array 
*/
class Graphe
{
    public:
    
        /**
         * @public The default constructor for the `Graphe` class
         * @param width of the array altitude
         * @param height of the array altitude
        */
        Graphe(int width, int height);
        
        /**
         * @public The destructor for the Table class.
        */
        ~Graphe();

		
		void display() const;
		void displayColor() const;

		void dijkstra(int start,int end);
		void dfs(int start);
		void bfs(int start);
		int* getWhiteAdj(int start);
		
				int getNorthFrom(int position);
		int getSouthFrom(int position);
		int getEastFrom(int position);
		int getWestFrom(int position);
		
    private:
    
		void setAltitudeCellToZero();
		void setColorCellToZero();
		
		int* getTwoDimArrayCoordFrom(int position);
		int getOneDimArrayCoordFrom(int aWidth,int aHeight);

		int getDist(int coord1,int coord2);
		void generateHills();
		
		

		
		
        int width;
        int height;
        int* altitude;
        int* color;

};

#endif

