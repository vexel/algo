#ifndef SKIPLISTELEMENT_HPP
#define SKIPLISTELEMENT_HPP

class SkipListElement
{
    public:
        int mValue;

        SkipListElement( int n ) : mValue(n){};
        SkipListElement() : SkipListElement( 0 ){};

        bool operator<( const SkipListElement & sl );
        bool operator>( const SkipListElement & sl );
};

#endif