// LIFAP6 - Automne 2018

#include "element.h" //offrant le type Elem
#include "liste.h" //offrant le type liste
#include <iostream>


//Constructeurs-------------------------------------------------------------
Liste::Liste()
{
    ad = 0;
    taille = 0;
}
//Postcondition : la liste initialisee est vide
Liste::Liste(const Liste & l)
{
    taille = 0;
    if(l.ad == 0)
    {
        ad=0;
    }
    else
    {

        Cellule * celltmp = l.ad;
        ad = 0;

        for (int i = 0; i < l.taille; i++)
        {
            ajoutEnQueue(celltmp->info);
            celltmp = celltmp->suivant;
        }
    }
}
//Postcondition : la liste initialisee et l correspondent a des listes identiques
//                (mais elles sont totalement independantes l'une de l'autre)

//Destructeur---------------------------------------------------------------
Liste::~Liste(){
    //delete ...
}

//Affectation---------------------------------------------------------------
/*Liste& Liste::operator = (const Liste & l)
//Precondition : aucune
//               (la liste a affecter et l initialisees et manipulees uniquement
//               a travers les operations du module)
//Postcondition : la liste affectee et l correspondent a des listes identiques
//                (mais elles sont totalement independantes l'une de l'autre)

Liste::bool testVide() const
//Precondition : aucune
//               (*this initialisee et manipulee uniquement a travers les
//                operations du module)
//Resultat : true si *this est vide, false sinon

Liste::Elem premierElement() const
//Precondition : testListeVide(l)==false
//Resultat : valeur de l'Elem contenu dans la 1ere Cellule

Liste::Cellule * premiereCellule() const
//Precondition : aucune
//               (*this initialisee et manipulee uniquement a travers les
//                operations du module)
//Resultat : adresse de la premiere cellule de *this si this->testVide()==false
//           O sinon
//           Attention : la liste *this pourrait ensuite etre modifiee a travers
//           la connaissance de l'adresse de sa premiere cellule

Liste::Cellule * celluleSuivante(const Cellule *c) const
//Precondition : c adresse valide d'une Cellule de la Liste *this
//Resultat : adresse de la cellule suivante si elle existe
//           O sinon
//           Attention : la liste *this pourrait ensuite etre modifiee a travers
//           la connaissance de l'adresse d'une de ses cellules

Liste::Elem elementCellule(const Cellule * c) const
//Precondition : c adresse valide d'une Cellule de la Liste *this
//Resultat : valeur de l'Elem contenu dans la Cellule
*/
void Liste::affichage() const
{
    Cellule * tmp = ad;

    if (this->ad == 0)
    {
        std::cout<<"La liste est vide \n";
    }
    else
    {
        int T = taille;
        std::cout<<"L={"<<tmp->info;
        while (T > 1)
        {
            tmp = tmp->suivant;
            std::cout<<","<<tmp->info;
            T--;
        }
        std::cout<<"} \n";
    }
}

void Liste::ajoutEnTete(const Elem & e)
{
    Cellule * ptmp = new Cellule;

    ptmp->info = e;
    ptmp->suivant = ad;
    ad = ptmp;
    taille++;
}/*
//Precondition : aucune
//               (*this et e initialises et manipules uniquement a travers les
//                operations de leurs modules respectifs)
//Postcondition : L'Elem e est ajoute en tete de *this

Liste::void suppressionEnTete()
//Precondition : this->testVide()==false
//Postcondition : la liste *this perd son premier element

Liste::void vide()
//Precondition : aucune
//               (*this initialisee et manipulee uniquement a travers les
//                operations du module)
//Postcondition : this->testVide()==true*/

void Liste::ajoutEnQueue(const Elem & e)
{
    Cellule * c = new Cellule;
    c->info = e;
    c->suivant = 0;

    if (ad == 0)
    {
        ad = c;
    }
    else
    {
        Cellule * ptcell = ad;

        for(int i=1; i<taille; i++)
        {
            ptcell = ptcell->suivant;
        }

        ptcell->suivant = c;
    }
    taille++;
}

/*
//Precondition : aucune
//               (*this et e initialises et manipules uniquement a travers les
//                operations de leurs modules respectifs)
//Precondition : L'Elem e est ajoute en fin de la liste *this

//OPERATIONS QUI POURRAIENT ETRE AJOUTEES AU MODULE

Liste::Cellule * rechercheElement(const Elem & e) const
//Precondition : aucune
//               (*this initialisee et manipulee uniquement a travers les
//                operations du module)
//Resultat : Adresse de la premiere Cellule de *this contenant e, 0 sinon
//           Attention : la liste *this pourrait ensuite etre modifiee a travers
//           la connaissance de l'adresse d'une de ses cellules

Liste::void insereElementApresCellule(const Elem & e,Cellule *c)
//Precondition : c adresse valide d'une Cellule de la Liste *this
//               ou 0 si this->testVide()==true
//Postcondition : l'element e est insere apres la Cellule pointee par c

Liste::void modifieInfoCellule(const Elem & e,Cellule *c)
//Precondition : *this non vide et c adresse valide d'une Cellule de *this
//Postcondition : l'info contenue dans *c a pour valeur e


Liste::void ajoutEnQueueConnaissantUneCellule(const Elem & e, Cellule *c)

Liste::void affichageDepuisCellule(const Cellule * c) const
*/

