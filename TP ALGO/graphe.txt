int* Graphe::getTwoDimArrayCoordFrom(int position){
	int arr[2];
	arr[0] = position/this->width;
	arr[1] = position%this->width;
	return arr;
	
}

int Graphe::getOneDimArrayCoordFrom(int aWidth,int aHeight){
	return aWidth + (aHeight * this->width);
}



int Graphe::getNorthFrom(int position){
	int y = getTwoDimArrayCoordFrom(position)[0];
	int x = getTwoDimArrayCoordFrom(position)[1];
	if(y != 0)
		return getOneDimArrayCoordFrom(x,y-1);
	return -1;
}
int Graphe::getSouthFrom(int position){
	int y = getTwoDimArrayCoordFrom(position)[0];
	int x = getTwoDimArrayCoordFrom(position)[1];
	if(y != this->height-1)
		return getOneDimArrayCoordFrom(x,y+1);
	return -1;
}
int Graphe::getEastFrom(int position){
	int y = getTwoDimArrayCoordFrom(position)[0];
	int x = getTwoDimArrayCoordFrom(position)[1];
	if(x != this->width-1)
		return getOneDimArrayCoordFrom(x+1,y);
	return -1;
}
int Graphe::getWestFrom(int position){
	int y = getTwoDimArrayCoordFrom(position)[0];
	int x = getTwoDimArrayCoordFrom(position)[1];
	if(x != 0)
		return getOneDimArrayCoordFrom(x-1,y);
	return -1;
}




void Graphe::generateHills(){
	int hillsNumber = rand()%100;

	for(int i=0;i<hillsNumber;i++){	
		int origineX = rand()%this->width;
		int origineY = rand()%this->height;
		int pos = getOneDimArrayCoordFrom(origineX,origineY);
		
		int north = getNorthFrom(pos);
		int south = getSouthFrom(pos);
		int west = getWestFrom(pos);
		int east = getEastFrom(pos);
		
		if(north != -1)
			this->altitude[north]+=1;
		if(south != -1)
			this->altitude[south]+=1;
		if(east != -1)
			this->altitude[east]+=1;
		if(west != -1)
			this->altitude[west]+=1;
			
		
		
		this->altitude[pos] +=  2;
		

	}
	display();	
}

void Graphe::setAltitudeCellToZero(){
	for(int y=0;y<=this->height;y++){
		for(int x=0;x<=this->width;x++){
			this->altitude[x+y*width] = 0;
		}
	}
}

void Graphe::setColorCellToZero(){
	for(int y=0;y<=this->height;y++){
		for(int x=0;x<=this->width;x++){
			this->color[x+y*width] = 0;
		}
	}


Graphe::Graphe(int width,int height)
{
	this->width = width;
	this->height = height;
	this->altitude = new int[width*height];
	this->color = new int[width*height];
	setAltitudeCellToZero();
	setColorCellToZero();
	generateHills();

}