// LIFAP6 - Automne 2017 - R. Chaine

#include <cstdio>
#include "element.h" //offrant le type Elem
#include "liste.h"

Liste::Liste()
{
    Cellule * bidon = new Cellule;
    ad = bidon;

    taille = 0;
    bidon->info=0;
    for(int i = 0; i < sizeof(bidon->suivant);i++){
        bidon->suivant[i] = 0;
    }
}

bool Liste::testVide() const
{
    return ad==0; //this->ad==0;
}

Elem Liste::premierElement() const
{
    return ad->info; //this->ad->info;
}

Cellule * Liste::premiereCellule() const
{
    return ad; //this->ad;
}

Cellule * Liste::celluleSuivante(const Cellule *c) const
{
  return c->suivant[0];
}

Elem Liste::elementCellule(const Cellule * c) const
{
  return c->info;
}

#ifndef _RECURSIF
void Liste::affichage() const
{
  std::printf("Liste (it) ");
  Cellule *temp=ad; // Cellule *temp=this->ad;
  while(temp!=0)
    {
      affichageElement(temp->info);
      temp=temp->suivant[0];
    }
  std::putchar('\n');
}
#else

//Procedure interne au module
void LISTE::affichageDepuisCellule(const Cellule * c) const
{
  if(c!=0) //il reste des cellules a afficher
    {
      affichageElement(c->info);
      affichageDepuisCellule(c->suivant);//this->afficheDepuisCellule(c->suivant);
    }
}
void afficheListe() const
{
  std::printf("Liste (rec) :");
  affichageDepuisCellule(ad); // this->afficheDepuisCellule(this->ad);
  std::putchar('\n');
}
#endif

void Liste::ajoutEnTete(const Elem & e)
{
  //Creation d'une nouvelle Cellule
  Cellule *temp=new Cellule;
  temp->info=e;
  temp->suivant[0]=this->ad;//... qui pointe sur la premiere Cellule de *this
  ad=temp; // this->ad=temps; *temp devient la premiere Cellule de *this
}

void Liste::suppressionEnTete()
{
  Cellule *temp = ad; // temp=this->ad; On memorise l'adresse de la premiere Cellule
  ad=ad->suivant[0]; //this->ad=this->ad->suivant; La deuxieme Cellule de *this devient la premiere
  delete temp;//L'espace occupe par la Cellule abandonnee est restitue
}

#ifndef _RECURSIF
void Liste::vide()
{
  while(!testVide()) //while(!this->testVide())
    {
      suppressionEnTete(); //this->suppressionEnTete();
    }
}
#else
void Liste::vide()
{
  if(!testVide()) //  if(!this->testVide())
    {
      suppressionEnTete(); //this->suppressionEnTete()
      vide(); //this->vide()
    }
}
#endif

#ifndef _RECURSIF
void Liste::ajoutEnQueueConnaissantUneCellule(const Elem & e, Cellule *c)
// procedure interne au module :
// *this NON VIDE et c est l'adresse d'une Cellule de *this
{
  Cellule *temp=c;

  while(temp->suivant[0]!=0){

    temp=temp->suivant[0];
  }
  //temp pointe sur la derniere cellule

  Cellule* anotherTemp = new Cellule;
  printf("%s",e)
  temp->suivant[0]= anotherTemp;
  temp->suivant[0]->info=e;


  temp->suivant[0]->suivant[0]=0;


}
#else
void Liste::ajoutEnQueueConnaissantUneCellule(const Elem & e, Cellule *c)
//procedure interne au module :
// *this NON VIDE et c est l'adresse d'une Cellule de *this
{
  if(c->suivant==0)
    {
      c->suivant=new Cellule;
      c->suivant->info=e;
      c->suivant->suivant=0;
    }
  else
    this->ajoutEnQueueConnaissantUneCellule(e,c->suivant);
}
#endif

void Liste::ajoutEnQueue(const Elem & e)
{
printf("ok");
  if(this->testVide()){
      ;
      this->ajoutEnTete(e);
  }

  else{

    this->ajoutEnQueueConnaissantUneCellule(e,this->ad);
}
}


Liste::Liste(const Liste & l)
{
  this->ad=0;
  if(!l.testVide())
    {
      Cellule *temp1=l.ad;
      this->ajoutEnQueue(temp1->info);
      Cellule *temp2=this->ad;
      temp1=temp1->suivant[0];
      while(temp1!=0)
      { //Il reste des elements a ajouter
          this->ajoutEnQueueConnaissantUneCellule(temp1->info,temp2);
          temp2=temp2->suivant[0]; //Ainsi temp2 pointe sur la derniere cellule de *this
          temp1=temp1->suivant[0]; //tmp1 pointe sur la premiere Cellule de l
                                // dont l'info n'a pas ete ajoutee a *this
      }
    }
}

Liste::~Liste()
{
  this->vide();
}

Liste & Liste::operator=(const Liste & l)
{
  if (this!=&l)
    {
      this->vide();
      if(!l.testVide())
	{
	  Cellule *temp1=l.ad;
	  this->ajoutEnQueue(temp1->info);
	  Cellule *temp2=this->ad;
	  temp1=temp1->suivant[0];
	  while(temp1!=0)
	    { //Il reste des elements a ajouter
	      this->ajoutEnQueueConnaissantUneCellule(temp1->info,temp2);
	      temp2=temp2->suivant[0]; //Ainsi temp2 pointe sur la derniere cellule de *this
	      temp1=temp1->suivant[0];
	    }
	}
    }
  return *this;
}
