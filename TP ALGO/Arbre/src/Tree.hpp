/**
 * @file Tree.hpp
 * @authors Vincent KLEINBAUER
*/
#pragma once
/**
 * @class Tree
 * @brief A tree defined by a pointer to a Node
*/
class Tree
{
    public:
		Tree();
		
		Tree(Node * root);
		
		~Tree();
		
		void displayTree(int deepness) const;
		
		bool insertNode(Node * root) const;
		
		bool searchNode(Element e) const;
		
    private:
        Node * root;
        
        void destroyRecursive();
};
