/**
 * @file main.cpp
 * @authors Vincent KLEINBAUER
*/

#include <iostream>

#include "Element.hpp"
#include "Node.hpp"
#include "Tree.hpp"


int main(int argc, char ** argv)
{

    
    Node root(Element(55));Tree aTree(&root);
    Node node1(Element(10));aTree.insertNode(&node1);
    Node node2(Element(20));aTree.insertNode(&node2);
    Node node3(Element(19));aTree.insertNode(&node3);
    Node node4(Element(40));aTree.insertNode(&node4);
    Node node5(Element(3));aTree.insertNode(&node5);
    Node node6(Element(65));aTree.insertNode(&node6);
    Node node7(Element(80));aTree.insertNode(&node7);
    Node node8(Element(77));aTree.insertNode(&node8);
    Node node9(Element(90));aTree.insertNode(&node9);
    Node node10(Element(4));aTree.insertNode(&node10);
    Node node11(Element(57));aTree.insertNode(&node11);
    Node node12(Element(56));aTree.insertNode(&node12);
    Node node13(Element(58));aTree.insertNode(&node13);
    Node node14(Element(1));aTree.insertNode(&node14);
    Node node15(Element(1));aTree.insertNode(&node15);

    aTree.displayTree(0);
	std::cout<<aTree.searchNode(Element(10))<<std::endl;
    return 0;
}
