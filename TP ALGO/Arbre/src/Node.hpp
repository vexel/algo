/**
 * @file Node.hpp
 * @authors Vincent KLEINBAUER
*/
#pragma once
/**
 * @class Node
 * @brief A tree is a finite number of junction between nodes
*/
class Node
{
    public:
        Node();
        
        Node(Element data);
 
        Node(Element data,Node *rightSon,Node *leftSon);

        ~Node();
                        
        Node * getLeftSon() const;
        
        Node * getRightSon() const;
        
        Element getData() const;
        
        void display() const;
        
        void setLeftSon(Node *leftSonPointer);
        
        void setRightSon(Node *rightSonPointer);

    private:
        Element data;
        Node *leftSon;
        Node *rightSon;
};
