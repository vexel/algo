/**
 * @file Tree.h
 * @authors Vincent KLEINBAUER
*/
#include <iostream>
#include <string>
#include "Element.hpp"
#include "Node.hpp"
#include "Tree.hpp"

/**
 * @class Tree
 * @brief A tree defined by a pointer to a Node
*/

Tree::Tree(){
		this->root = nullptr;
}

Tree::Tree(Node * root){
	this->root = root;
}

Tree::~Tree(){
	destroyRecursive();
}

void Tree::destroyRecursive(){
	if((*this->root).getLeftSon() != nullptr){
		Tree subTree((*this->root).getLeftSon());
		subTree.destroyRecursive();
	}
	if((*this->root).getRightSon() != nullptr){
		Tree subTree((*this->root).getRightSon());
		subTree.destroyRecursive();
	}
}

void Tree::displayTree(int deepness) const{

	deepness++;
	if((*this->root).getRightSon() != nullptr){
		Tree subTree=((*this->root).getRightSon());
		subTree.displayTree(deepness);
	}

	for(int i=0;i<deepness;i++){
		std::cout<<" - ";
	}
	Element anElement = (*this->root).getData();
	std::cout<<anElement.getInfo()<<std::endl<<std::endl;
	
	if((*this->root).getLeftSon() != nullptr){
		Tree subTree=((*this->root).getLeftSon());
		subTree.displayTree(deepness);
	}
	
}

bool Tree::insertNode(Node * pointerToInsert) const{
	
	// son on right and info < node.elem
	if( (*this->root).getRightSon() != nullptr && (*this->root).getData().getInfo() < (*pointerToInsert).getData().getInfo()){
		Tree subTree=(*this->root).getRightSon();
		return subTree.insertNode(pointerToInsert);
	}
	
	// son on left and info > node.elem
	if((*this->root).getLeftSon() != nullptr && (*this->root).getData().getInfo() > (*pointerToInsert).getData().getInfo()){
		Tree subTree=(*this->root).getLeftSon();
		return subTree.insertNode(pointerToInsert);
	}
	
	// no son
	else {
		
		// info > node.elem.info
		if((*this->root).getLeftSon() == nullptr  && (*this->root).getData().getInfo() > (*pointerToInsert).getData().getInfo()){
			(*this->root).setLeftSon(pointerToInsert);
			return true;
		}
		
		// info < node.elem.info
		if((*this->root).getRightSon() == nullptr  && (*this->root).getData().getInfo() < (*pointerToInsert).getData().getInfo()){
			(*this->root).setRightSon(pointerToInsert);
			return true;
		}
		
		// info = node.elem.info
		if((*this->root).getData().getInfo() == (*pointerToInsert).getData().getInfo()){
			std::cout<<"impossible element "<<(*pointerToInsert).getData().getInfo()<<" already in the tree"<<std::endl;
			return false;
		}
	}
}


bool Tree::searchNode(Element e) const{
	if((*this->root).getData().getInfo() == e.getInfo()){
		return (*this->root).getData().getInfo() == e.getInfo();
	}
	
	if((*this->root).getData().getInfo() < e.getInfo()){
		Tree subTree=(*this->root).getRightSon();
		if((*this->root).getRightSon()== nullptr){
			return false;
		}
		return subTree.searchNode(e);
	}

	
	if((*this->root).getData().getInfo() > e.getInfo()){
		Tree subTree=(*this->root).getLeftSon();
		if((*this->root).getLeftSon()==nullptr){
			return false;
		}
		return subTree.searchNode(e);
	}
}



