/**
 * @file Element.hpp
 * @authors Vincent KLEINBAUER
*/
#pragma once
/**
 * @class Element
 * @brief Element is the information of the node
*/
class Element
{
    public:
        Element();
        
        ~Element();
        
        Element(int info);
        
        int getInfo() const;

    private:
        int information;
};
