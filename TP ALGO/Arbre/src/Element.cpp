/**
 * @file Element.cpp
 * @authors Vincent KLEINBAUER
*/
#include <iostream>
#include "Element.hpp"

Element::Element(){
	this-> information = 0;
}

Element::~Element(){
	this->information = 0;
}

Element::Element(int info){
	this->information = info;
}

int Element::getInfo() const{
	return this->information;
}
