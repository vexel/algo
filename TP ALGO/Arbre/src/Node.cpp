/**
 * @file Node.h
 * @authors Vincent KLEINBAUER
*/

#include <iostream>
#include "Element.hpp"
#include "Node.hpp"


/**
 * @class Node
 * @brief A tree is a finite number of junction between nodes
*/
Node::Node(){
	this->data = Element();
	this->leftSon = nullptr;
	this->rightSon = nullptr;
}

Node::Node(Element data){
	this->data = data;	
	this->leftSon = nullptr;
	this->rightSon = nullptr;
}

Node::Node(Element data,Node *rightSon,Node *leftSon){
	this->data = data;
	this->leftSon = leftSon;
	this->rightSon = rightSon;
}

Node::~Node(){
	//pass	
}
				
Node * Node::getLeftSon() const{
	return this->leftSon;
}

Node * Node::getRightSon() const{
	return this->rightSon;
}

Element Node::getData() const{
	return this->data;
}

void Node::display() const{

}

void Node::setLeftSon(Node *leftSonPointer){
	this->leftSon = leftSonPointer;
}
        
void Node::setRightSon(Node *rightSonPointer){
	this->rightSon = rightSonPointer;
}
