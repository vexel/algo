// LIFAP6 - Automne 2018

#include "element.h" //offrant le type Elem
#include "liste.h" //offrant le type liste
#include <iostream>


//Constructeurs-------------------------------------------------------------
Liste::Liste()
{
    Cellule * bidon = new Cellule;
    ad = bidon;

    taille = 0;
    bidon->info=0;
    bidon->suivant=0;
}
//Postcondition : la liste initialisee est vide
Liste::Liste(const Liste & l)
{
    taille = 0;
    if(l.ad->suivant == 0)
    {
        Cellule * bidon = new Cellule;
        ad = bidon;
        ad->suivant = 0;
    }
    else
    {
        Cellule * celltmp = l.ad->suivant;
        Cellule * bidon = new Cellule;
        ad = bidon;

        for (int i = 0; i < l.taille; i++)
        {
            ajoutElement(celltmp->info);
            celltmp = celltmp->suivant;
        }
    }
}

//Destructeur---------------------------------------------------------------
Liste::~Liste()
{
    //this->vide();
}

//Affectation---------------------------------------------------------------
/*Liste & Liste::operator=(const Liste & l) //Correction prof
{
  if (this!=&l)
    {
      this->vide();
      if(!l.testVide())
	{
	  Cellule *temp1=l.ad;
	  this->ajoutEnQueue(temp1->info);
	  Cellule *temp2=this->ad;
	  temp1=temp1->suivant;
	  while(temp1!=0)
	    { //Il reste des elements a ajouter
	      this->ajoutEnQueueConnaissantUneCellule(temp1->info,temp2);
	      temp2=temp2->suivant; //Ainsi temp2 pointe sur la derniere cellule de *this
	      temp1=temp1->suivant;
	    }
	}
    }
  return *this;
}

bool Liste::testVide() const
{
    return ad==0;
}

Elem Liste::premierElement() const
{
    return ad->info; //this->ad->info;
}

Cellule * Liste::premiereCellule() const
{
    return ad->suivant; //this->ad;
}

Cellule * Liste::celluleSuivante(const Cellule *c) const
{
  return c->suivant;
}

Elem Liste::elementCellule(const Cellule * c) const
{
  return c->info;
}*/

void Liste::affichage() const
{
    Cellule * tmp = ad->suivant;


    if (ad->suivant == 0)
    {
        std::cout<<"La liste est vide \n";
    }
    else
    {
        int T = taille;
        std::cout<<"L={"<<tmp->info;
        while (tmp->suivant!=0 && T > 1)
        {
            tmp = tmp->suivant;
            std::cout<<","<<tmp->info;
            T--;
        }
        std::cout<<"} \n";
    }
}

void Liste::ajoutElement(const Elem & e)
{
    Cellule * newcell = new Cellule;
    Cellule * ptcell = ad;

    newcell->info = e;

    if(ptcell->suivant == 0)
    {
        ptcell->suivant=newcell;
        newcell->suivant=0;
    }
    else
    {
        while(ptcell->suivant!=0 && e < ptcell->suivant->info)
        {
            ptcell = ptcell->suivant;
        }

        //ajout de la céllule
        newcell->suivant=ptcell->suivant;
        ptcell->suivant=newcell;
    }

    taille++;
}
/*
void Liste::suppressionEnTete()
{
    Cellule *temp = ad; // temp=this->ad; On memorise l'adresse de la premiere Cellule
    ad=ad->suivant; //this->ad=this->ad->suivant; La deuxieme Cellule de *this devient la premiere
    delete temp;
}

void Liste::vide()
{
    while(!testVide()) //while(!this->testVide())
    {
      suppressionEnTete(); //this->suppressionEnTete();
    }
}

void Liste::ajoutEnQueue(const Elem & e)
{
    Cellule * c = new Cellule;
    c->info = e;
    c->suivant = 0;

    if (ad == 0)
    {
        ad = c;
    }
    else
    {
        Cellule * ptcell = ad;

        for(int i=1; i<taille; i++)
        {
            ptcell = ptcell->suivant;
        }

        ptcell->suivant = c;
    }
    taille++;
}


//Precondition : aucune
//               (*this et e initialises et manipules uniquement a travers les
//                operations de leurs modules respectifs)
//Precondition : L'Elem e est ajoute en fin de la liste *this

//OPERATIONS QUI POURRAIENT ETRE AJOUTEES AU MODULE

Cellule * Liste::rechercheElement(const Elem & e) const
//Precondition : aucune
//               (*this initialisee et manipulee uniquement a travers les
//                operations du module)
//Resultat : Adresse de la premiere Cellule de *this contenant e, 0 sinon
//           Attention : la liste *this pourrait ensuite etre modifiee a travers
//           la connaissance de l'adresse d'une de ses cellules

void Liste::insereElementApresCellule(const Elem & e,Cellule *c)
//Precondition : c adresse valide d'une Cellule de la Liste *this
//               ou 0 si this->testVide()==true
//Postcondition : l'element e est insere apres la Cellule pointee par c

void Liste::modifieInfoCellule(const Elem & e,Cellule *c)
//Precondition : *this non vide et c adresse valide d'une Cellule de *this
//Postcondition : l'info contenue dans *c a pour valeur e*/


/*
void Liste::ajoutEnQueueConnaissantUneCellule(const Elem & e, Cellule *c)
// procedure interne au module :
// *this NON VIDE et c est l'adresse d'une Cellule de *this
{
  Cellule *temp=c;
  while(temp->suivant!=0)
    temp=temp->suivant;
  //temp pointe sur la derniere cellule
  temp->suivant=new Cellule;
  temp->suivant->info=e;
  temp->suivant->suivant=0;
}*/

//void Liste::affichageDepuisCellule(const Cellule * c) const


